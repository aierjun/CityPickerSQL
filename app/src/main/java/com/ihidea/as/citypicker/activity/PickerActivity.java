package com.ihidea.as.citypicker.activity;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;

import com.ihidea.as.citypicker.R;
import com.ihidea.as.citypicker.picker.PickerDialogView;

/**
 * Created by Ani_aierJun on 2018/2/26.
 */

public class PickerActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_picker);
        findView();
    }

    private void findView() {
        PickerDialogView pickerDialogView = new PickerDialogView(this);
        pickerDialogView.show();
    }
}
