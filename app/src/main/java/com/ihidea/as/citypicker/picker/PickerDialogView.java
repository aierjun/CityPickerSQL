package com.ihidea.as.citypicker.picker;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.NumberPicker;

import com.aierjun.weelview.wheel.WheelView;
import com.aierjun.weelview.wheel.adapters.ArrayWheelAdapter;
import com.ihidea.as.citypicker.R;
import com.lljjcoder.DataLoad;
import com.lljjcoder.bean.ProvinceBean;

import java.util.List;

/**
 * Created by Ani_aierJun on 2018/2/26.
 */

public class PickerDialogView extends Dialog{
    private Context mContext;
    private View view;

    public PickerDialogView(@NonNull Context context) {
        this(context,0);
    }

    public PickerDialogView(@NonNull Context context, int themeResId) {
        super(context, themeResId);
        mContext = context;
        view = View.inflate(context, R.layout.dialog_picker,null);
        setContentView(view);
        findView();
    }

    private void findView() {
        if (view == null) return;

        List<String> list2 = DataLoad.getProvincetoData(mContext);
        String[] s = new String[list2.size()];
        for (int i = 0; i < list2.size(); i++) {
            Log.d("Ani", list2.get(i) + " ... 省");
            s[i] = list2.get(i);
        }

        WheelView picker = (WheelView) view.findViewById(R.id.picker);
        ArrayWheelAdapter arrayWheelAdapter = new ArrayWheelAdapter<String>(mContext, s);
        picker.setViewAdapter(arrayWheelAdapter);
    }


}
